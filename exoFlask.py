from flask import Flask
from moduleSquare import square

app = Flask(__name__)

@app.route('/')
def welcome():
    return "Bienvenue sur ma page d'accueil"

@app.route("/square/<number>")
def computeSquare(number):
    try :
        n = float(number)
    except:
        return "Problème ..."
    try:
        res=square(n)
    except:
        return "Problème ..."

    return (f"Le carré de {number} est {res}")

app.run(host="0.0.0.0", port=8083)