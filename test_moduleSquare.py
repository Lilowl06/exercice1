import unittest
from moduleSquare import square

class TestModuleSquare(unittest.TestCase):
    def test_square(self):
        self.assertEqual(square(3), 9)
    
    def test_types(self):
        # self.assertRaises(TypeError, square, True)
        self.assertRaises(TypeError, square, "coucou")
